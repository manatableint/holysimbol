"use strict"
exports.__esModule = true

//var main = require('./main')
var exec = require('child_process').exec
var writeJson = require('write-json')

function ImageSearch_xy(file, rate, x1, y1, x2, y2, cb) {
    var cmd = "ImageSearch.exe" + " " + file + " " + rate + " " + x1 + " " + y1 + " " + x2 + " " + y2
    var process = exec(cmd, function(error,stdout,stderr) {
        if(stdout) {
            var array = stdout.toString().split('\r\n')
            writeJson.sync('xy.json', {x: array[1], y: array[2]})
        }
        cb()
    })
}

function ImageSearch(file, rate, x1, y1, x2, y2, false_callback, true_callback) {
    var cmd = "ImageSearch.exe" + " " + file + " " + rate + " " + x1 + " " + y1 + " " + x2 + " " + y2
    var process = exec(cmd, function(error,stdout,stderr) {
        if(stdout) true_callback()
        else false_callback()
    })
}

function ImageSearch_true(file, rate, x1, y1, x2, y2, true_callback) {
    var cmd = "ImageSearch.exe" + " " + file + " " + rate + " " + x1 + " " + y1 + " " + x2 + " " + y2
    var process = exec(cmd, function(error,stdout,stderr) {
	//console.log(file)
        if(stdout) return true_callback()
        else process.kill
    })
}

function ImageSearch_false(file, rate, x1, y1, x2, y2, false_callback) {
    var cmd = "ImageSearch.exe" + " " + file + " " + rate + " " + x1 + " " + y1 + " " + x2 + " " + y2
    var process = exec(cmd, function(error,stdout,stderr) {
        if(stdout) process.kill
        else return false_callback()
    })
}

module.exports = {
    xy: ImageSearch_xy,
    default: ImageSearch,
    true: ImageSearch_true,
    false: ImageSearch_false
}