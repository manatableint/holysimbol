﻿var imagesearch = require('./imagesearch')
var RJ = require('self-reload-json')
var status = new RJ('./status.json')
var key = new RJ('./key.json')
var writeJson = require('write-json')
var robot = require('robotjs')
var sleep = require('sleep-js')
var xy = new RJ('./xy.json')

function mp_check(cb) {
    imagesearch.false("image/mana.png", 50, 500, 500, 800, 768, ()=>{
        console.log('MP가 낮습니다.')
	robot.keyTap(key.potion)
        //writeJson.sync('status.json', {status: '4', target_channel: status.target_channel, channel: status.channel})
        })
    cb()
}

function partymember_check() {
    if(status.status != 2) {
    imagesearch.true("image/party_health.png", 30, 200, 400, 1000, 600, ()=>{
        writeJson.sync('status.json', {status: '2', target_channel: status.target_channel, channel: status.channel})
        console.log('근처에 파티원이 있습니다.')
        var time = Math.floor(Math.random() * 1000) + 500
        sleep(time).then( () => {
            robot.keyTap(key.simbol)
            mp_check(() => {
                time = Math.floor(Math.random() * 4000) + 3000
                sleep(time).then(() => {
                    writeJson.sync('status.json', {status: '0', channel: status.channel, target_channel: status.target_channel})
                    var main = require('./main')
                    sleep(500).then(() => {main.check()})
                })
            })
        } )
        })
    }
}

function invite_check() {
    imagesearch.true("image/party_invite.png", 30, 500, 500, 800, 768, ()=>{
        console.log('파티 신청을 받았습니다.')
        writeJson.sync('status.json', {status: '3', target_channel: status.target_channel, channel: status.channel})
        imagesearch.xy("image/invite_yes.png", 30, 400, 400, 900, 768, () => {
            sleep(500).then(() => {
                robot.moveMouse(xy.x, xy.y)
		sleep(100).then(() => { robot.mouseClick() })
		sleep(200).then(() => {robot.moveMouse(xy.x - 50, xy.y - 50)})
                writeJson.sync('status.json', {status: '0', target_channel: status.target_channel, channel: status.channel})
                var main = require('./main')
                sleep(500).then(() => {main.check()})
            })
        })
    })
}

module.exports = {
    mp: mp_check,
    member: partymember_check,
    invite: invite_check
}