var imagesearch = require('./imagesearch')

function signus_back_check() {
    imagesearch.false("image/signus_back.png", 30, ()=>{
        console.log('화면 확인 바랍니다.')
        })
}

function signus_mapname_check() {
    imagesearch.false("image/signus_mapname.png", 50, ()=>{
        console.log('캐릭터가 시그너스 맵에서 이동된 것 같습니다.')
        })
}

setInterval(signus_back_check,3000)
setInterval(signus_mapname_check,3000)