﻿var sleep = require('sleep-js')
var RJ = require('self-reload-json')
var chat = require('./chat_check')
var status_check = require('./status_check')
var status = new RJ('./status.json')
var key = new RJ('./key.json')
var robot = require('robotjs')
var writeJson = require('write-json')

var time

function check() {
    if(status.status == 0) {
        //process.stdout.write('\033c')
        console.log('현재 '+ status.channel + '채널 에서 대기중.')
        status_check.invite()
        if(status.status != 2) status_check.member()
        chat.check(() => {
            sleep(5000).then(() => { if(status.status == 0) check() })
        })
    }
    if(status.status == 1) {
        console.log('요청받은 채널로 이동합니다.')
        var change = status.target_channel - status.channel
        if(change > 0) {
            robot.keyTap("escape")
            robot.keyTap("enter")
            var move = change
            if(status.channel == 1) move = move + 1
            for(var i = move; i > 0; i--)
                robot.keyTap("right")
            sleep(500).then(() => {
                robot.keyTap("enter")
                writeJson.sync('status.json', {status: '0', channel: status.target_channel, target_channel: status.target_channel})
                sleep(5000).then(() => {
                    check()
                })
            })
        }
        else if(change < 0) {
            robot.keyTap("escape")
            robot.keyTap("enter")
            var move = Math.abs(change)
            if(status.target_channel == 1) move = move + 1
            for(var i = move; i > 0; i--)
                robot.keyTap("left")
            sleep(500).then(() => {
                robot.keyTap("enter")
                writeJson.sync('status.json', {status: '0', channel: status.target_channel, target_channel: status.target_channel})
                sleep(4000).then(() => {
                    check()
                })
            })
        }
        else {
            writeJson.sync('status.json', {status: '0', channel: status.target_channel, target_channel: status.target_channel})
            sleep(1000).then(() => {
                check()
            })
        }
    }
    if(status.status == 4) {
        robot.keyTap(key.potion)
        writeJson.sync('status.json', {status: '0', target_channel: status.channel, channel: status.channel})
        sleep(500).then(() => { check() })
    }
}

module.exports = {
    check: check
}

console.log('화면을 오른쪽 아래 끝에 붙여야함. 해상도 1024 768')
check()