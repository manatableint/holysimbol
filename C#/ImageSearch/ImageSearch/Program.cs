﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows;

namespace ImageSearch
{
    class Program
    {
        [DllImport("ImageSearchDLL.dll")]
        public static extern IntPtr ImageSearch(int x, int y, int right, int bottom, [MarshalAs(UnmanagedType.LPStr)]string imagePath);

        public static string[] UseImageSearch(string imgPath, string tolerance, string x1, string y1, string x2, string y2)
        {
            int i_x1 = Convert.ToInt32(x1);
            int i_y1 = Convert.ToInt32(y1);
            int i_x2 = Convert.ToInt32(x2);
            int i_y2 = Convert.ToInt32(y2);

            imgPath = "*" + tolerance + " " + imgPath;

            IntPtr result = ImageSearch(i_x1, i_y1, i_x2, i_y2, imgPath);
            string res = Marshal.PtrToStringAnsi(result);

            if (res[0] == '0') return null;

            string[] data = res.Split('|');

            int x; int y;
            int.TryParse(data[1], out x);
            int.TryParse(data[2], out y);

            return data;
        }

        static void Main(string[] args)
        {
                string[] results = UseImageSearch(args[0], args[1], args[2], args[3], args[4], args[5]);
                if (results != null)
                {
                    Console.WriteLine(results[0]);
                    Console.WriteLine(results[1]);
                    Console.WriteLine(results[2]);
                }
        }
    }

}
